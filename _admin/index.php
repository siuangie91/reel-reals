<?php 

require_once("_includes/header.php"); 
require_once("_includes/nav.php"); 

?>

<?php if(!$session->is_signed_in()) {$db_object->redirect("../");} ?>   
    
    <!--nav-->
<?php require_once("_includes/delete_modal.php"); ?>   
    <div class="content-wrapper" id="featuredvids-page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">Featured Vids <a href="add_featured_vid.php" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add Featured Vid</a></h1>
                </div>
            </div>
            <div class="row">
                
                <div class="col-md-12">
                    <?php

                    $featured_vids = Featured_vids::find_by_query("SELECT * FROM featured_vids ORDER BY id DESC");

                    foreach($featured_vids as $featured_vid) :
                    ?>

                    <div class="vid-tn featured-vid-tn col-md-5">
                        <div class="row text-center col-md-12 vid-header">
                            <div class="col-md-12 vid-title">
                                <h4><?php echo $featured_vid->id.') '.$featured_vid->title; ?></h4>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 text-center vid-btn">
                                <a href="edit_featured_vid.php?id=<?php echo $featured_vid->id; ?>" class="btn btn-info btn-block"><i class="fa fa-pencil"></i>Edit</a>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 vid-btn">
<!--                                <a href="delete_vid.php?id=<?php /*echo $featured_vid->id;*/ ?>&type=feat" class="btn btn-danger btn-block"><i class="fa fa-trash"></i>Delete</a>-->
                                
                                <a get="delete_vid.php?type=feat&id=" get-param="<?php echo $featured_vid->id; ?>" type="Featured Video" class="btn btn-danger btn-block delete-btn" name="<?php echo $featured_vid->title; ?>"><i class="fa fa-trash"></i>Delete</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <iframe src="<?php echo $featured_vid->url; ?>" width="100%" height="250px" frameborder="0" allowfullscreen></iframe>
                            <div class="vid-desc">
                            <?php echo $featured_vid->description; ?></div>
                        </div>
                    </div>

                    <?php 

                    endforeach;

                    ?>
                </div>
            </div>
            
           
            
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->


<?php require_once("_includes/footer.php"); ?>