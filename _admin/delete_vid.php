<?php 

require_once("_includes/header.php"); 

if(empty($conn->escape_string($_GET['id'])) || empty($conn->escape_string($_GET['type']))) {
    $db_object->redirect("./");
} else {
    
    if($_GET["type"] == "reel") {
        $vid_to_delete = Videos::find_by_id($_GET['id']);
        $redirect_to = "reels.php";
    } else if ($_GET["type"] == "feat") {
        $vid_to_delete = Featured_vids::find_by_id($_GET['id']);
        $redirect_to = "./";       
    } else {
        $db_object->redirect("../");    
    }
    
    if($vid_to_delete) {
        $vid_to_delete->delete();
        $vid_to_delete->redirect($redirect_to);
    } else {
        $vid_to_delete->redirect($redirect_to);
    }
}


?>