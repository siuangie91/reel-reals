<?php 

require_once("_includes/header.php"); 
require_once("_includes/nav.php"); 

?>
   
<?php if(!$session->is_signed_in()) {$db_object->redirect("../");} ?>
    
    <!--nav-->    
    <div class="content-wrapper" id="user_settings-page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">User Settings
                    <a href="./" class="btn btn-default pull-right"><i class="fa fa-long-arrow-left"></i> Return</a></h1>

                </div>
            </div>
        
            <?php
            $user = User::find_by_id($session->user_id);
            
            if(isset($_POST['update'])) {
                User::update_user($_POST['username'], $_POST['firstname'], $_POST['lastname'], $_POST['password'], $_POST['password_confirm']);    
            
                $session->message("<div class='alert alert-success alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                    <p>User Setting Updated Successfully!</p></div>");
                
                $db_object->redirect("user_settings.php");
            
            }
            
            echo $session->message;

            ?>
                   
            <div class="row">
                <div class="col-md-12">
                    <div id="error"></div> 
                    
                    <form action="" method="post">
                        <div class="form-group">
                            <label for=""><i class="fa fa-asterisk"></i> Username</label>
                            <input type="text" name="username" class="form-control" value="<?php echo $user->username; ?>">
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-asterisk"></i> First Name</label>
                            <input type="text" name="firstname" class="form-control" value="<?php echo $user->firstname; ?>">
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-asterisk"></i> Last Name</label>
                            <input type="text" name="lastname" class="form-control" value="<?php echo $user->lastname; ?>">
                        </div>
                        
                        <h3>Change Password</h3>
                        <div class="form-group">
                            <label for="">Set New Password</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="form-group">
                            <label for="">Confirm New Password</label>
                            <input type="password" class="form-control" name="password_confirm">
                        </div>
                        
                        <input type="submit" class="btn btn-warning" name="update" value="Update">
                        <a href="./" class="pull-right">Cancel</a>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->

    <script src="../_components/js/_validate.js"></script>
    <script>    
    $(document).ready(function(){
    
        var reqFields = [
                $('input[name="username"]'), 
                $('input[name="firstname"]'),
                $('input[name="lastname"]') 
            ];
        
        var msg = 'Username, First Name, and Last Name cannot be blank.';

        validate(reqFields, $('#error'), $('input[name="update"]'), msg);

        $('input[name="update"]').click(function(event){

            if($('input[name="password"]').val() !== "" && $('input[name="password"]').val() !== null) {
                if($('input[name="password_confirm"]').val() !== $('input[name="password"]').val()) {
                    event.preventDefault(); 
                    $('#error')/*.addClass('alert').addClass('alert-danger')*/.html('<b><i class="fa fa-fw fa-exclamation-triangle"></i>&nbsp; Passwords do not match! <i class="fa fa-fw fa-exclamation-triangle"></i></b>');
                } 

            } 

        });

        $('#error').css({
            color: "#ff0000",
            background: '#FFD6D6'
        });

    }); // end document ready
    </script>


<?php require_once("_includes/footer.php"); ?>