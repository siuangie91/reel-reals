tinymce.init({ selector:'textarea' });

$(document).ready(function() {
    ////////////////HIDE NAV BAR ON LOGIN PAGE///////////////
    if($('#login-page').length > 0) {
        $('a.navbar-brand').attr('href', 'login.php');
        $('header').hide();    
    }
    
    
    ////////////////ACTIVE NAV HIGHLIGHTING//////////////
    (function() {
        var pageName = $('.content-wrapper').attr('id').split('-')[0];    
                
        var activeLink;
        
        if(pageName == "featuredvids") {
            activeLink = $('li>a[href="./"]');  
        } else {
            activeLink = $('li>a[href="' + pageName + '.php"]');      
        }
        
        activeLink.addClass('menu-top-active');
    }());
    
    /////////////////SHOW PREVIEW OF VIDEO////////////////////
    (function() {
        if($('#url')) {
            var url = $('#url');
        }
        
        url.on('change blur', function() {
            $('#vid-preview').css("color", "inherit");
            $('#vid-preview').find('iframe').remove();
            
            if(url.val().match("^http")) {
                $.ajax({
                    url: "http://projects.angiesiudevworks.com/realreels/_/preview_vid",
                    data: {
                        url: url.val()
                    },
                    type: "POST",
                    success: function(data) {                    
                        if(!data.error) {
                            $('#vid-preview').html(data);
                        } else {
                            $('#vid-preview').html("Could not fetch preview at this time. Sorry!");    
                        }
                    }
                });
            } else {
                $('#vid-preview').html("<h3><i class='fa fa-exclamation-triangle'></i> invalid source code. <i class='fa fa-exclamation-triangle'></i></h3><br><h4>please use the src param of the embed url.</h4>").css("color", "#ff0000");
            }
        });
    }());
    
    ////////////////SHOW MORE VIDEOS IN ADMIN REELS//////////////
    (function() {
        //get vids, total num vids, and show more button
        var vids = $('.vid-tn');
        var currentNumVids = vids.length;
        console.log("currentNumVidsInAdmin: " + currentNumVids); 
        
        var showMore = $('#admin-show-more');
        
        showMore.click(function() {
            $.ajax({
                url: "http://projects.angiesiudevworks.com/realreels/_/show_more_vids",
                data: {
                    currentNumVidsInAdmin: currentNumVids   
                },
                type: "POST",
                success: function(data) {
                    if(!data.error) {
                        $('.vid-tn-container').append(data);
                        currentNumVids = $('.vid-tn').length;
                        console.log("currentNumVidsInAdmin: " + currentNumVids);  

                        $.ajax({
                            url: "http://projects.angiesiudevworks.com/realreels/_/show_more_vids",
                            data: {
                                updateCurrentNumVids: currentNumVids   
                            },
                            type: "POST",
                            success: function(data) {
                                if(!data.error) {
                                    if($.trim(data) === "No more videos") {
                                        showMore.html(data).attr('disabled', 'disabled');
                                    }
                                } else {
                                    $('.vid-tn-container').append("Error!");    
                                }
                            }
                        });
                    } else {
                        $('.vid-tn-container').append("Could not fetch videos at this time. Sorry!");    
                    }
                }
            });
            showMore.blur();
        });
    }());
    
    //////////////DELETE MODAL//////////////////
    $('.content-wrapper').on('click', '.delete-btn', function(){
        var id = $(this).attr('get-param');
        var get_var = $(this).attr('get');
        var delete_url = get_var + id;
        $(".modal-delete-link").attr("href", delete_url);

        var item_type = $(this).attr('type');

        var item_name = $(this).attr('name');

        $('#delete-modal').modal('show');
        $('.delete-item-name').text(item_type + ' #' + id + ": " + item_name);

        $('.modal-title i').css('color', '#ff0000');

    });
});