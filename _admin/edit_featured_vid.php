<?php 

require_once("_includes/header.php"); 
require_once("_includes/nav.php"); 

?>
   
<?php if(!$session->is_signed_in()) {$db_object->redirect("../");} ?>   
    <!--nav-->
<?php

if(empty($conn->escape_string($_GET['id']))) {
    $db_ob->redirect("../");    
} else {
    $the_feat_vid = Featured_vids::find_by_id($conn->escape_string($_GET['id']));    
}

?>    
    <div class="content-wrapper" id="featuredvids-page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line"><i class="fa fa-pencil"></i> <?php echo $_GET['id'].')&nbsp;&nbsp;'.$the_feat_vid->title; ?> 
                    <a href="./" class="btn btn-default pull-right"><i class="fa fa-long-arrow-left"></i> Return to Featured Vids</a></h1>
                </div>
            </div>
            <?php 
            
            if(isset($_POST['update_vid'])) {
                $the_feat_vid->title = $_POST['title']; 
                $the_feat_vid->url = $_POST['url'];
                $the_feat_vid->description = $_POST['description']; 

                $the_feat_vid->save();
                
                $session->message("<div class='alert alert-success alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                    <p><em><strong>$the_feat_vid->title</strong></em> Updated Successfully!</p>
                    <a href='../' target='_blank' class='color-link'><i class='fa fa-external-link'></i> View in Front End</a>
                </div>");
                
                $db_object->redirect("edit_featured_vid.php?id=$the_feat_vid->id");
                
            }
            
            echo $session->message;
            ?>

            <div class="row">
                <div class="col-md-6">
                    <div id="error"></div>
                    
                    <form action="" method="post">
                        <div class="form-group">
                            <label for=""><i class="fa fa-asterisk"></i>Title:</label>
                            <input type="text" class="form-control" name="title" value="<?php echo $the_feat_vid->title; ?>">
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-asterisk"></i>Src from Embed URL:</label>
                            <input type="text" class="form-control" name="url" id="url" value="<?php echo $the_feat_vid->url; ?>">
                        </div>
                        <div class="form-group">
                            <label for="">Description:</label>
                            <textarea name="description" id="vid-description" cols="30" rows="5" class="form-control"><?php echo $the_feat_vid->output_sql_text($the_feat_vid->description); ?></textarea>
                        </div>
                        <input type="submit" class="btn btn-warning" value="Update Featured Vid" name="update_vid">
                        <a href="./" class="pull-right">Cancel</a>
                    </form>
                </div>
                
                <div class="col-md-6 text-center">
                    <div id="vid-preview"></div>
                </div>
            </div>
            
           
            
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <script src="../_components/js/_validate.js"></script>
    <script>    
    $(document).ready(function(){
    
        var reqFields = [
                $('input[name="title"]'), 
                $('input[name="url"]') 
            ];
        
        validate(reqFields, $('#error'), $('input[name="update_vid"]'));

    }); // end document ready
    </script>

<?php require_once("_includes/footer.php"); ?>