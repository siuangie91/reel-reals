<?php require_once("_includes/header.php"); ?>

<?php

if($session->is_signed_in()) {
    $db_object->redirect("./");
}

?>
   
   
    <div class="content-wrapper" id="login-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Please Login To Enter </h4>
                </div>
            
                <div id="login-error" class="col-md-6 col-md-offset-3 text-center">
                    <?php 
                
                    if(isset($_POST['login'])) {
                        $username = $conn->escape_string(trim($_POST['username']));
                        $password = $conn->escape_string(trim($_POST['password']));

                        //method to check db user
                        $user_found = User::verify_user($username, $password);

                        if($user_found) {
                            $user_found->set_user_session_settings($user_found);
                            $session->login($user_found);
                            $db_object->redirect("./");
                        } else {
                            echo "<i class='fa fa-warning'></i> Incorrect username/password combination. <i class='fa fa-warning'></i>";
                    ?>

                        <script>
                            $('#login-error').css({
                                background: '#FFD6D6',
                                color: '#ff0000'
                            });
                        </script> 
                    <?php
                        }
                    } else {
                        $username = "";
                        $password = "";
                    }

                    ?>
                </div>
            
            </div>
            <div class="row">
                <form class="col-md-6 col-md-offset-3" action="" method="post">

                    <label><i class="fa fa-asterisk"></i>Enter Username : </label>
                    <input type="text" class="form-control" name="username"/>
                    <label><i class="fa fa-asterisk"></i>Enter Password : </label>
                    <input type="password" class="form-control" name="password"/>
                    <br>
                    <div class="row text-center">
                        <input type="submit" class="btn btn-warning" name="login" value="Log In">
                    </div>
                    <br>
                    <div class="col-md-12 text-center">
                        <a href="../"><i class="fa fa-long-arrow-up"></i> Return to Site</a>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <script src="../_components/js/_validate.js"></script>
    <script>    
    $(document).ready(function(){
        $('input[name="username"]').focus();
        
        var reqFields = [
                $('input[name="username"]'), 
                $('input[name="password"]') 
            ];
        
        var msg = 'Please enter both your username and password.';
        
        validate(reqFields, $('#login-error'), $('input[name="login"]'), msg);

    }); // end document ready
    </script>


<?php require_once("_includes/footer.php"); ?>