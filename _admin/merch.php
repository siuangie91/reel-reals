<?php 

require_once("_includes/header.php"); 
require_once("_includes/nav.php"); 

?>
    
<?php if(!$session->is_signed_in()) {$db_object->redirect("../");} ?>    
    <!--nav-->
    
    <div class="content-wrapper" id="merch-page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">Manage Merchandise
                    <a href="./" class="btn btn-default pull-right"><i class="fa fa-long-arrow-left"></i> Return</a></h1>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                
                    <p>If a Prestashop website were set up, the user would've been directed to the Prestashop login.</p>
                
                </div>
            </div>
            
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->


<?php require_once("_includes/footer.php"); ?>