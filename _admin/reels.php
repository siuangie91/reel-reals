<?php 

require_once("_includes/header.php"); 
require_once("_includes/nav.php"); 

?>
    
<?php if(!$session->is_signed_in()) {$db_object->redirect("../");} ?>    
    <!--nav-->
<?php require_once("_includes/delete_modal.php") ?>    
    
    <div class="content-wrapper" id="reels-page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">Latest Reels<a href="add_reel.php" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add Reel</a></h1>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12 vid-tn-container">

                    <?php
                    
                    $reels = new Videos();
                    $num_show = $reels->num_vids_to_show + 1;
                    
                    $videos = Videos::find_by_query("SELECT * FROM videos ORDER BY upload_time DESC, id DESC LIMIT {$num_show}");
                    
                    //ORDER BY upload_time DESC, id DESC
                    
                    foreach($videos as $video) :
                    
                    ?>
                    
                    <div class="vid-tn col-md-4">
                        <div class="row text-center col-md-12 vid-header">
                            <div class="col-md-12 vid-title">
                                <h4><?php echo $video->id.') '.$video->title; ?></h4>
                                <?php echo $video->formatted_date(); ?>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 text-center vid-btn">
                                <a href="edit_reel.php?id=<?php echo $video->id; ?>" class="btn btn-info btn-block"><i class="fa fa-pencil"></i>Edit</a>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 vid-btn">
                                <a get="delete_vid.php?type=reel&id=" get-param="<?php echo $video->id; ?>" type="Reel" class="btn btn-danger btn-block delete-btn" name="<?php echo $video->title; ?>"><i class="fa fa-trash"></i>Delete</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <iframe src="<?php echo $video->url; ?>" width="100%" height="250px" frameborder="0" allowfullscreen></iframe>
                            <div class="vid-desc">
                            <?php echo $video->description; ?></div>
                        </div>
                    </div>    
                    
                    <?php 
                    
                    endforeach;
                    
                    ?>
                    
                </div>
                
                <div class="text-center">
                    <button class="btn btn-primary" id="admin-show-more">Show Older</button>
                </div>
                
            </div>
            
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->


<?php require_once("_includes/footer.php"); ?>