<?php 

require_once("_includes/header.php"); 
require_once("_includes/nav.php"); 

?>
    
<?php if(!$session->is_signed_in()) {$db_object->redirect("../");} ?>    
    <!--nav-->
    
    <div class="content-wrapper" id="featuredvids-page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line"><i class="fa fa-plus"></i> Add a Featured Vid <a href="./" class="btn btn-default pull-right"><i class="fa fa-long-arrow-left"></i> Return to Featured Vids</a></h1>
                </div>
            </div>
            
            <?php 
            
            $vid = new Featured_vids();
            
            if(isset($_POST['add_feat_vid'])) {
                if($vid) {
                    $vid->title = $_POST['title'];
                    $vid->description = $_POST['description'];  
                    $vid->url = $_POST['url'];
                    
                    $vid->save();
             
                    $session->message("<div class='alert alert-success alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                        <p><em><strong>$vid->title</strong></em> Added Successfully!</p>
                        <a href='../' target='_blank' class='color-link'><i class='fa fa-external-link'></i> View in Front End</a>
                    </div>");

                    $db_object->redirect("add_featured_vid.php");
                }
                
            }
            
            echo $session->message;
            ?>
            
            <div class="row">
                <div class="col-md-6">
                    <div id="error"></div>
                    
                    <form action="" method="post">
                        <div class="form-group">
                            <label for=""><i class="fa fa-asterisk"></i>Title:</label>
                            <input type="text" class="form-control" name="title">
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-asterisk"></i>Src from Embed URL:</label>
                            <input type="text" class="form-control" name="url" id="url">
                        </div>
                        <div class="form-group">
                            <label for="">Description:</label>
                            <textarea name="description" id="vid-description" cols="30" rows="5" class="form-control"></textarea>
                        </div>
                        <input type="submit" class="btn btn-warning" value="Add Featured Vid" name="add_feat_vid">
                        <a href="./" class="pull-right">Cancel</a>
                    </form>
                </div>
                
                <div class="col-md-6 text-center">
                    <div id="vid-preview"></div>
                </div>
            </div>
            
           
            
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <script src="../_components/js/_validate.js"></script>
    <script>    
    $(document).ready(function(){
    
        var reqFields = [
                $('input[name="title"]'), 
                $('input[name="url"]') 
            ];
        
        validate(reqFields, $('#error'), $('input[name="add_feat_vid"]'));

    }); // end document ready
    </script>

<?php require_once("_includes/footer.php"); ?>