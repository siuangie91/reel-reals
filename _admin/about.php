<?php 

require_once("_includes/header.php"); 
require_once("_includes/nav.php"); 

?>
    
<?php if(!$session->is_signed_in()) {$db_object->redirect("../");} ?>
    <!--nav-->
    
    <div class="content-wrapper" id="about-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">About Page
                    <a href="./" class="btn btn-default pull-right"><i class="fa fa-long-arrow-left"></i> Return</a></h1>

                </div>
            </div>
            <div class="row">
                <?php 
                
                $about_texts = About_text::find_all(); 
                
                if(isset($_POST['update'])) {
                    $about_texts[0]->description = $_POST['blurb'];
                    
                    $about_texts[0]->save();
                
                    $session->message("<div class='alert alert-success alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                        <p>Blurb Updated Successfully!</p>
                        <a href='../about.php' target='_blank' class='color-link'><i class='fa fa-external-link'></i> View in Front End</a>
                    </div>");

                    $db_object->redirect("about.php");

                }

                echo $session->message;
                ?>

                <form action="" method="post" class="col-md-6 col-md-offset-3">
                    <div class="form-group">
                        <textarea name="blurb" id="" cols="30" rows="5"><?php echo $about_texts[0]->output_sql_text($about_texts[0]->description); ?></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-warning" value="Update" name="update">
                        <a href="./" class="pull-right">Cancel</a>
                    </div>
                </form>
            </div>
            
           
            
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->


<?php require_once("_includes/footer.php"); ?>