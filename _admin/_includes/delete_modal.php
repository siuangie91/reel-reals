<!-- Modal -->
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-fw fa-warning"></i> Confirm Delete</h4>
      </div>
      <div class="modal-body">
          <p class='text-center'>Are you sure you want to delete<br><strong><span class="delete-item-name"></span></strong>?</p> 
      </div>
      <div class="modal-footer">
        <a href="" class="btn btn-danger modal-delete-link">Yes</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>