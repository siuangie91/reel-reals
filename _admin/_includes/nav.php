<section class="menu-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navbar-collapse collapse ">
                    <ul id="menu-top" class="nav navbar-nav navbar-left">
                        <li><a href="./">Featured Vids</a></li>
                        <li><a href="reels.php">Latest Reels</a></li>
                        <li><a href="about.php">About</a></li>
                        <li><a href="merch.php">Manage Merchandise</a></li>
                    </ul>
                    <ul id="user-menu" class="nav navbar-nav navbar-right">
                        <li><a href="../"><i class="fa fa-long-arrow-up"></i> Return to Site</a></li>
                        <li><a href="user_settings.php"><i class="fa fa-user"></i> User Settings</a></li>
                        <li><a href="logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>