<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6 text-center">
                        &copy; 2016 Real Reels, a <em>fictitious</em> company
                    </div>
                    <div class="col-md-6 text-center">
                        Powered by <a href="http://angiesiudevworks.com/" target="_blank"><i class="fa fa-fw fa-terminal"></i>Angie Siu </a>
                    </div>
                </div>

            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    
    
    <script src="assets/js/admin_script.js"></script>
</body>
</html>