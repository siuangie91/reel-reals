$('.carousel').carousel({
    interval: false
});

$(document).ready(function() {
    
/*    
        ////////////NAVBAR SM ICON STYLES//////////
        $('#nav-right>li>a>i')
        .mouseenter(function() {
            var icon = $(this).attr('class').substr(12).split('-')[0];

            if(icon === 'envelope') {
                $(this).append('<span> message</span>');    
            } else {
                $(this).append('<span> ' + icon + '<span>');       
            }
        
            $(this).find('span').css({
                fontSize: '1.25rem',
                color: '#fff'
            });
        })
        .mouseleave(function() {
            $(this).find('span').remove();
        });*/
    ////////////////ACTIVE NAV STYLE//////////////
    (function() {
        var pageName = $('.page-content').attr('id').split('-')[0];    
                
        var activeLink = $('#nav-left>li>a[href="' + pageName + '.php"]');
        
        activeLink.addClass('active-nav');
    }());
    
    ////////////////VALIDATE CONTACT FORM//////////////
    (function() {
        var reqFields = [$('#contact-form input:not("#frm-submit")'), $('#contact-form textarea')];
        
        validate(reqFields, $('#error'), $('#frm-submit'));
    }());
    
    ////////////////SHOW MORE VIDEOS//////////////
    (function() {
        //get vids, total num vids, and show more button
        var vids = $('.video-container');
        var currentNumVids = vids.length;
        console.log("Total vids: " + currentNumVids); 
        
        var showMore = $('#show-more');
        var goToYT = $('#go-to-YT');
        goToYT.hide();
        
        showMore.click(function() {
            $.ajax({
                url: "http://projects.angiesiudevworks.com/realreels/_/show_more_vids",
                data: {
                    currentNumVids: currentNumVids   
                },
                type: "POST",
                success: function(data) {
                    if(!data.error) {
                        $('.videos').append(data);
                        
                        currentNumVids = $('.video-container').length;
                        
                        console.log("Total vids: " + currentNumVids);  

                        $.ajax({
                            url: "http://projects.angiesiudevworks.com/realreels/_/show_more_vids",
                            data: {
                                updateCurrentNumVids: currentNumVids   
                            },
                            type: "POST",
                            success: function(data) {
                                if(!data.error) {
                                    if($.trim(data) === "No more videos") {
                                        showMore.hide();
                                        goToYT.show();
                                    }
                                } else {
                                    $('.videos').append("Error!");    
                                }
                            }
                        });
                    } else {
                        $('.videos').append("Could not fetch videos at this time. Sorry!");    
                    }
                }
            });
            showMore.blur();
        });
    }());
    
}); //end document ready
