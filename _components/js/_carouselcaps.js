(function() {    
    var carouselDescs = $('.carousel-desc');
    carouselDescs.hide();
    carouselDescs.eq(0).show();
    
    var carouselIndicators = $('.carousel-indicators li')
//    console.log(carouselIndicators.length);
    
    carouselIndicators.attrchange({        
        trackValues: true,
        callback: function(event) {
//            console.log(event.newValue);    
            if(event.newValue === 'active') {
                var indicator = $(this).index();    
                carouselDescs.eq(indicator).show();
                $('.carousel-desc:lt('+indicator+')').hide();
                $('.carousel-desc:gt('+indicator+')').hide();
//                console.log(indicator);
            }
        }
    });
}());