//form validation master function     
//fields is an array
function validate(fields, alertElem, submitElem, alertString) {
    
    alertString = alertString || 'Required fields cannot be blank.'    
        
    submitElem.click(function(event){
        
        for (var i = 0; i < fields.length; i++) {
            //clear all previous highlighting
            fields[i].css({
                'box-shadow': 'none',
                'border': '1px solid #CCC'
            });
            
            if (fields[i].val() == "" || fields[i].val() == null) {
                event.preventDefault();
                if(fields[i].val() == "") {
                    fields[i].css({
                        'box-shadow': '0px 0px 5px #ff0000',
                        'border': '1px solid #ff0000'
                    });
                    console.log('Field ' + (i+1) + ' was empty');
                }
                alertElem.html('<span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;'+alertString+'&nbsp;<span class="glyphicon glyphicon-exclamation-sign"></span>');
                alertElem.css({
                    color: '#ff0000',
                    fontWeight: 'bold'
                });
                submitElem.blur();
                $(window).scrollTop(alertElem.offset().top - 150);
            } 
        } 
    });
}