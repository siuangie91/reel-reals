    
<!DOCTYPE html>
<html lang="en">
    
<head>
    <title>Shop 100% Real Stuff | Real Reels</title>    
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
    <?php require_once('_includes/head.php'); ?>
</head>

<body>
<?php require_once('_includes/nav.php'); ?>

<div class="page-content" id="merchandise-page-content">

    <div id="placeholder">
        <h1>Merchandise Coming Soon!</h1>
        <img src="_assets/logo-round.gif" alt="Real Reels" class="logo-round">
    </div>

</div> <!--- Page content --->

<?php require_once('_includes/footer.php');
