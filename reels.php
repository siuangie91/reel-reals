<!DOCTYPE html>
<html lang="en">
    
<head>
    <title>Latest 100% Real Videos | Real Reels</title>    
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
    <?php require_once('_includes/head.php'); ?>
</head>

<body>
<?php require_once('_includes/nav.php'); ?>

<div class="page-content" id="reels-page-content">

    <h1>LATEST REELS</h1>
    
    <div class="videos">
        <?php 

        $videos = new Videos();

        $reels = Videos::find_by_query("SELECT * FROM videos ORDER BY upload_time DESC, id DESC LIMIT {$videos->num_vids_to_show}");

        foreach($reels as $reel) :

        ?>

        <div class="video-container">
            <div class="video-info">
                <h2 class="video-name"><?php echo $reel->title; ?></h2>
                <span class="video-date"><?php echo $reel->upload_time; ?></span>
            </div>
            <div class="video">
                <iframe src="<?php echo $reel->url; ?>" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="video-desc"><?php echo $reel->description; ?></div>
        </div>

        <?php

        endforeach;

        ?>

    </div>
    
    <button class="btn wide-btn" id="show-more"><i class="fa fa-plus"></i> Show Older Videos</button>
    
    <a href="http://youtube.com" target="_blank" class="btn wide-btn" id="go-to-YT">See More on My YouTube Channel! <i class="fa fa-smile-o"></i></a>

</div> <!--- Page content --->

<?php require_once('_includes/footer.php');
