<?php

class User extends Db_object {
    
    protected static $db_table = "users";
    protected static $db_table_fields = array('username', 'password', 'firstname', 'lastname');
    
    public $id;
    public $username;
    public $password;
    public $firstname;
    public $lastname;
    public $updated_user = false;
    
    public static function verify_user($username, $password) {
        global $conn;
        
        $username = $conn->escape_string($username);
        $password = $conn->escape_string($password);
    
        $sql = "SELECT * FROM " . self::$db_table . " WHERE username = '{$username}' LIMIT 1";
        
        $result_array = self::find_by_query($sql);
        
//        print_r($result_array);
//        
//        echo "<br><br><br>".$result_array[0]->password; 
        
        if(!empty($result_array)) {
            $db_password = $result_array[0]->password;   
            
            return password_verify($password, $db_password) ? array_shift($result_array) : false;
        }
    }
    
    public function set_user_session_settings($user) {
        if($user) {
            $this->id = $user->id;
            $this->username = $user->username;
            $this->password = $user->password;
            $this->firstname = $user->firstname;
            $this->lastname = $user->lastname;
        }
    }

    public static function update_user($username, $firstname, $lastname, $password, $password_confirm) {
        global $conn;
        
        $username = $conn->escape_string($username);
        $password = $conn->escape_string($password);
        $password_confirm = $conn->escape_string($password_confirm);
        $firstname = $conn->escape_string($firstname);
        $lastname = $conn->escape_string($lastname);
        
        $sql = "UPDATE users SET ";
        $sql.= "username = '{$username}', ";
        $sql.= "firstname = '{$firstname}', ";
        $sql.= "lastname = '{$lastname}' ";
        
        if(empty($password) || $password == null) {
            $sql.= " WHERE id = {$_SESSION['user_id']}";
        } else if (!empty($password) && !empty($password_confirm) && $password === $password_confirm) {
            $password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
            
            $sql.= ", password = '{$password}' ";
            $sql.= "WHERE id = {$_SESSION['user_id']}";
//            $sql.= "WHERE id = 1";
        }
        
        $update_query = $conn->query($sql);
        
        $_SESSION['user_name'] = $username;
        $_SESSION['user_password'] = $password;
        $_SESSION['user_firstname'] = $firstname;
        $_SESSION['user_lastname'] = $lastname;
        
    }
    
//    public static function logout() {
//        session_destroy();
//        self::redirect("/");
//    }
    
} // end of user class

$user = new User();
?>




