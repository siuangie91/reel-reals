<?php


class Db_object {
    
    public $upload_errors_array = array(
        UPLOAD_ERR_OK           => "There is no error",
        UPLOAD_ERR_INI_SIZE     => "The uploaded file exceeds the upload_max_filesize directive",
        UPLOAD_ERR_FORM_SIZE    => "The uploaded file exceeds the MAX_FILE_SIZE directive",
        UPLOAD_ERR_PARTIAL      => "The uploaded file was only partially uploaded.",
        UPLOAD_ERR_NO_FILE      => "No file was uploaded.",
        UPLOAD_ERR_NO_TMP_DIR   => "Missing a temporary folder.",
        UPLOAD_ERR_CANT_WRITE   => "Failed to write file to disk.",
        UPLOAD_ERR_EXTENSION    => "A PHP extension stopped the file upload."
    );
    
    public static function find_all() {
        return static::find_by_query("SELECT * FROM " . static::$db_table . "");
    }
    
    public static function find_by_id($id) {
        global $conn;
        
        $result_array = static::find_by_query("SELECT * FROM " . static::$db_table . " WHERE id = {$id} LIMIT 1");
        
        return !empty($result_array) ? array_shift($result_array) : false;
    }
    
    public static function find_by_query($sql) {
        global $conn;
        
        $result_set = $conn->query($sql);
        $the_obj_array = array();
        
        while($row = mysqli_fetch_array($result_set)) {
            $the_obj_array[] = static::instantiation($row);
        }
        return $the_obj_array;
    }
    
    public static function instantiation($the_record) {
        $calling_class = get_called_class();
        
        $the_object = new $calling_class;
        
        foreach ($the_record as $the_attr => $value) {
            if($the_object->has_attr($the_attr)) {
                $the_object->$the_attr = $value;    
            }
        }

        
        return $the_object;
    }
    
    private function has_attr($attr) {
        $obj_properties = get_object_vars($this);//get props of given obj
        
        return array_key_exists($attr, $obj_properties); //returns boolean
    }
    
    protected function properties() {
//        return get_object_vars($this);    
        $properties = array();
        
        foreach (static::$db_table_fields as $db_field) {
            if(property_exists($this, $db_field)) {
                $properties[$db_field] = $this->$db_field;
            }
        }
        
        return $properties;
    }
    
    protected function clean_properties() {
        global $conn;
        
        $clean_properties = array();
        
        foreach($this->properties() as $key => $value) {
            $clean_properties[$key] = $conn->escape_string($value);    
        }
        
        return $clean_properties;
    }
    
    public function output_sql_text($field) {
        $to_be_replaced = array('\n', '\r', '\\\'', '\\');
        $replace_with = array('', '', '\'', '');
        echo str_replace($to_be_replaced, $replace_with, $field);
    }
    
    public function redirect($location) {
        header("Location: $location");
    }
    
    public function save() {
        return isset($this->id) ? $this->update() : $this->create();
    }
    
    public function create() {
        global $conn;
        
        $properties = $this->clean_properties();//returns assoc array
        
        //implode() joins array elements with a specified "glue" (the first param)
        $sql = "INSERT INTO ".static::$db_table." (". implode(",", array_keys($properties)) .")";
        $sql.= " VALUES('". implode("','", array_values($properties)) ."')";
        
        if($conn->query($sql)) {
            $this->id = $conn->insert_id();
            
            return true;    
        } else {
            return false;
        }

    }
    
    public function update() {  
        global $conn;
        
        $properties = $this->clean_properties();
        
        $properties_pairs = array();
        
        foreach($properties as $key => $value) {
            $properties_pairs[] = "{$key} = '{$value}'";
        }
        
        $sql = "UPDATE ".static::$db_table." SET ";
        $sql.= implode(", ", $properties_pairs);
        $sql.= " WHERE id = ". $conn->escape_string($this->id);

        $conn->query($sql);
        
        return (mysqli_affected_rows($conn->connection) == 1) ? true : false;
    }
    
    public function delete() {  
        global $conn;
        
        $sql = "DELETE FROM ".static::$db_table." WHERE id = ". $conn->escape_string($this->id) . " LIMIT 1";

        $conn->query($sql);
        
        return (mysqli_affected_rows($conn->connection) == 1) ? true : false;
    }
}

$db_object = new Db_object();

?>