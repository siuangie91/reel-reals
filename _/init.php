<?php

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

require_once("config.php"); 
require_once("conn.php"); 
require_once("db_obj.php");
require_once("videos.php");
require_once("featured_vids.php");
require_once("about_text.php");
require_once("user.php");
require_once("session.php");

?>