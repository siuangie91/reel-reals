<?php

class Featured_vids extends Db_object {
    protected static $db_table = "featured_vids";
    protected static $db_table_fields = array('id', 'title', 'description', 'url');

    public $id;
    public $title;
    public $description;
    public $url;
    
}

?>