<?php

require("init.php");

if(isset($_POST['updateCurrentNumVids'])) {
    $numVids = Videos::find_all();
    
    if($_POST['updateCurrentNumVids'] >= count($numVids)) {
        
        echo "No more videos";    
        
    }
}

if(isset($_POST['currentNumVids'])) {
    $videos = new Videos();
        
    $reels = Videos::find_by_query("SELECT * FROM videos ORDER BY upload_time DESC LIMIT {$videos->num_vids_to_show} OFFSET {$_POST['currentNumVids']}");

    foreach($reels as $reel) {

        echo "<div class='video-container'>";
        echo "<div class='video-info'>";
        echo "<h2 class='video-name'>{$reel->title}</h2>";
        echo "<span class='video-date'>{$reel->upload_time}</span>";
        echo "</div>";
        echo "<div class='video'>";
        echo "<iframe src='$reel->url' frameborder='0' allowfullscreen></iframe>";
        echo "</div>";
        echo "<div class='video-desc'>$reel->description</div>";
        echo "</div>";

    }
}

if(isset($_POST['currentNumVidsInAdmin'])) {
    $videos = new Videos();
    $num_vids_to_show = $videos->num_vids_to_show + 1;
        
    $reels = Videos::find_by_query("SELECT * FROM videos ORDER BY upload_time DESC, id DESC LIMIT {$num_vids_to_show} OFFSET {$_POST['currentNumVidsInAdmin']}");

    foreach($reels as $reel) {

        echo "<div class='vid-tn col-md-4'>";
        echo "<div class='row text-center col-md-12 vid-header'>";
        echo "<div class='col-md-12 vid-title'>";
        echo "<h4>$reel->id) $reel->title</h4>";
        echo $reel->formatted_date() . "</div>";
        echo "<div class='col-md-6 col-sm-6 col-xs-6 text-center vid-btn'>";
        echo "<a href='edit_reel.php?id=$reel->id' class='btn btn-info btn-block'><i class='fa fa-pencil'></i>Edit</a></div>";
        echo "<div class='col-md-6 col-sm-6 col-xs-6 vid-btn'>";
        echo "<a get='delete_vid.php?type=reel&id=' get-param='$reel->id' type='Reel' class='btn btn-danger btn-block delete-btn' name='$reel->title'><i class='fa fa-trash'></i>Delete</a></div></div>";
        echo "<div class='col-md-12'>";
        echo "<iframe src='$reel->url' width='100%' height='250px' frameborder='0' allowfullscreen></iframe><div class='vid-desc'>";
        echo $reel->description . "</div></div></div>";
    }
}

?>


