<?php

class Videos extends Db_object {
    protected static $db_table = "videos";
    protected static $db_table_fields = array('id', 'title', 'description', 'upload_time', 'url');

    public $id;
    public $title;
    public $description;
    public $upload_time;
    public $url;
    
    public $current_num_vids;
    public $num_vids_to_show;
    public $total_num_vids;
    
    public function __construct($current_num_vids = 5, $total_num_vids = 0, $num_vids_to_show = 5) {
        $this->current_num_vids = (int)$current_num_vids;
        $this->total_num_vids = (int)$total_num_vids;
        $this->num_vids_to_show = (int)$num_vids_to_show;
    }
    
    public function formatted_date() {
        $this->upload_time = date("M. j, Y", strtotime($this->upload_time));    
        
        return $this->upload_time;
    }
    
//    public function format_date($time) {
//        return date("M. j, Y", strtotime($time));
//    }
    
    public function add() {
        global $conn;
        
        
    }
    
    
    public function create() {
        global $conn;
        
        $properties = $this->clean_properties();//returns assoc array
        
        //implode() joins array elements with a specified "glue" (the first param)
        $sql = "INSERT INTO ".static::$db_table." (". implode(",", array_keys($properties)) .")";
        $sql.= " VALUES('". implode("','", array_values($properties)) ."')";
        
        if($conn->query($sql)) {
            $this->id = $conn->insert_id();
            
            return true;    
        } else {
            return false;
        }

    }
}

?>