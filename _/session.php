<?php

class Session {
    
    private $signed_in = false;
    public $user_id;
    public $user_name;
    public $user_password;
    public $user_firstname;
    public $user_lastname;
    public $message;
    
    function __construct() {
        session_start();
        $this->check_login();
        $this->is_signed_in();
        $this->check_message();
    }
    
    public function message($msg="") {
        if(!empty($msg)) {
            $_SESSION['message'] = $msg;    
        } else {
            return $this->message;    
        }
    }
    
    private function check_message() {
        if(isset($_SESSION['message'])) {
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message = "";    
        }
    }
    
    public function is_signed_in() {
        return $this->signed_in;
        
        
    }
    
    public function login($user) {   
        if($user) {
            $this->user_id = $_SESSION['user_id'] = $user->id; /*user_id from the instance*/
            
            $this->user_name = $_SESSION['user_name'] = $user->username;
            
            $this->user_password = $_SESSION['user_password'] = $user->password;
            
            $this->user_firstname = $_SESSION['user_firstname'] = $user->firstname;
            
            $this->user_lastname = $_SESSION['user_lastname'] = $user->lastname;
            
            $this->signed_in = true;
        }
    }
    
    public function logout() {
        unset($_SESSION['user_id']);    
        unset($this->user_id);
        session_destroy();
        $this->signed_in = false;
    }
    
    private function check_login() {
        if(isset($_SESSION['user_id'])) {
            $this->user_id = $_SESSION['user_id'];
//            $this->user_name = $_SESSION['user_name'];
//            
//            $this->user_password = $_SESSION['user_password'];
//            
//            $this->user_firstname = $_SESSION['user_firstname'];
//            
//            $this->user_lastname = $_SESSION['user_lastname'];
            
            $this->signed_in = true;
        } else {
            unset($this->user_id);    
            $this->signed_in = false;
        }
    }
    
}

$session = new Session();


?>