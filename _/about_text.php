<?php

class About_text extends Db_object {
    protected static $db_table = "about";
    protected static $db_table_fields = array('id', 'description');

    public $id;
    public $description;
    
}

?>