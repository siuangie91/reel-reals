<!DOCTYPE html>
<html lang="en">
    
<head>
    <title>Real Reels | 100% Real Videos</title>    
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
    <?php require_once('_includes/head.php'); ?>
</head>

<body>
<?php require_once('_includes/nav.php'); ?>
<?php $featured_vids = Featured_vids::find_by_query("SELECT * FROM featured_vids ORDER BY id DESC"); ?>
<div class="page-content" id="home-page-content">

    <h1><i class="fa fa-ellipsis-h"></i> 100% Real Videos! <i class="fa fa-ellipsis-h"></i></h1>
    <h2><!--<small>(on everything!)</small>-->Featured Reels</h2>

    <!--*******************CAROUSEL********************-->
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php
            
            $indicator_counter = 0;
            
            foreach($featured_vids as $featured_vid) :
            
            ?>
            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $indicator_counter; ?>" class="<?php if($indicator_counter == 0) {echo "active";} else {echo " ";} ?>"></li>
            <?php
            
            $indicator_counter++;
            endforeach;
            
            ?>

        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
        
            <?php 
            $vid_counter = 1;
            
            foreach($featured_vids as $featured_vid) : 
                
            ?>
            <div class="item <?php if($vid_counter <= 1) {echo "active";} ?>">
                <iframe src="<?php echo $featured_vid->url; ?>" frameborder="0" allowfullscreen></iframe>
            </div>
            <?php 
            $vid_counter++;
            endforeach; 
            ?>
<!--
            <div class="item active">
                <iframe src="https://www.youtube.com/embed/ey2kb4jIIs4" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="item">
                <iframe src="https://www.youtube.com/embed/nKQvL-jzvvw?list=RDnKQvL-jzvvw" frameborder="0" allowfullscreen></iframe>
            </div>
-->
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!--*******************END CAROUSEL********************-->

    <div class="carousel-descriptions">
        
        <?php foreach($featured_vids as $featured_vid) : ?>
        
        <div class="carousel-desc" id="carousel-desc-<?php echo $featured_vid->id; ?>">
            <h3><?php echo $featured_vid->title; ?></h3>
            <div><?php echo $featured_vid->description; ?></div>
        </div>
        
        <?php endforeach; ?>
    </div>

</div> <!--- Page content --->

<?php require_once('_includes/footer.php');
