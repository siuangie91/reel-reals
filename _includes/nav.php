<div class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="slide-nav">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="navbar-brand" href="./"><!--<img src="http://placehold.it/200x100&text=Logo+200x100" alt="Logo"><--><img src="_assets/logo-light.gif" alt="Real Reels" width="100%"></--></a>
        </div>
        <div id="slidemenu">

            <ul class="nav navbar-nav pull-left" id="nav-left">
                <li><a href="reels.php">Latest Reels</a></li>
                <li><a href="about.php">About Us</a></li>
                <li><a href="merchandise.php">Merchandise</a></li>
            </ul>

            <ul class="nav navbar-nav pull-right" id="nav-right">
                <li><a href="http://youtube.com" target="_blank" title="YouTube"><i class="fa fa-fw fa-youtube-square"></i></a></li>
                <li><a href="http://facebook.com" target="_blank" title="Facebook"><i class="fa fa-fw fa-facebook-square"></i></a></li>
                <li><a href="http://twitter.com" target="_blank" title="Twitter"><i class="fa fa-fw fa-twitter-square"></i></a></li>
                <li><a href="http://tumblr.com" target="_blank" title="Tumblr"><i class="fa fa-fw fa-tumblr-square"></i></a></li>
                <li><a href="about.php#contact-anchor" id="nav-contact" title="Message"><i class="fa fa-fw fa-envelope-square"></i></a></li>
            </ul>

        </div>
    </div>
</div>
    
