<?php 

require_once "./PHPMailer/PHPMailerAutoload.php";

$included_files = get_included_files();

//foreach ($included_files as $filename) {
//    echo "$filename\n";
//}
$mail = new PHPMailer();
$name="";
$email="";
$subject="";
$message="";

if (isset($_POST["submit"])) {
    if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
        //secret key
        $secret = getenv("RECAPTCHA_KEY");
        //get verify response data
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        
        if($responseData->success) {
            $name=strip_tags($_POST["name"]);
            $email=strip_tags($_POST["email"]);
            $subject=strip_tags($_POST["subject"]);
            $message=strip_tags($_POST["message"]);


            $mail->SMTPDebug = 0;                               // Use 3 to Enable verbose debug output

            $mail->IsSMTP();                                      // Set mailer to use SMTP
            $mail->Host = "smtp.gmail.com";  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = getenv("PHPMAILER_EMAIL");                 // SMTP username
            $mail->Password = getenv("PHPMAILER_PASS");                           // SMTP password
            $mail->SMTPSecure = "ssl";                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to

            $mail->From = $email;
            $mail->FromName = $name;
            $mail->addAddress(getenv("PHPMAILER_SENDTO"));               // Name is optional 
            $mail->addReplyTo($email);
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = $subject;
            $mail->Body = "<u><strong>Name</strong></u>: ".$name."<br/><br/> <u><strong>Email</strong></u>: ".$email."<br/><br/> <u><strong>Subject</strong></u>: ".$subject."<br/><br/> <u><strong>Message (sent from Real Reels)</strong></u>: <br/><br/>".$message;

            if(!$mail->send()) {
                echo "Message could not be sent.";
                echo "Mailer Error: " . $mail->ErrorInfo;
                die("Mailer Error: " . $mail->ErrorInfo);
            } else {
                echo "Message has been sent";

                $session->message("<div class='modal fade' id='submit-modal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
                    <div class='modal-dialog modal-sm' role='document'>
                        <div class='modal-content'>
                            <div class='modal-header'>
                                <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                <h4 class='modal-title' id='modal-msg-submitted'><i class='fa fa-fw fa-paper-plane'></i> Message Submitted!</h4>
                            </div>
                            <div class='modal-body'>
                                <p>Thanks for the message, {$_POST['name']}! I'll get back to you soon about {$_POST['subject']}!</p>
                            </div>
                            <div class='modal-footer'>
                                <a href='about.php' class='btn btn-default'>OK!</a>
                            </div>
                        </div>
                    </div>
                </div>");

                header("Location: about.php");
            }
        }
    } else {
        $session->message("Please verify that you are not a robot.");    
    }
    
}
?>