-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 15, 2016 at 06:41 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `realreels`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `description`) VALUES
(1, '<p style="text-align: justify;">Hi everyone! This is my "channel" where I show my favorite videos about anything and everything! I hope you enjoy my selection. These videos are not doctored! They are real, raw videos of real things that happened.</p>\r\n<p style="text-align: justify;">&nbsp;</p>\r\n<p style="text-align: justify;">&nbsp;<em>**Obviously, the videos posted here aren''t "real" or "raw." They were used for the sake of having videos to embed.</em></p>\r\n<p style="text-align: justify;">&nbsp;</p>\r\n<p style="text-align: justify;"><em>However, the contact form below&nbsp;</em><span style="text-decoration: underline;">is</span>&nbsp;<em>real so you can most definitely contact me from here!</em></p>\r\n<p style="padding-left: 30px; text-align: justify;">&nbsp;</p>\r\n<p style="padding-left: 30px;">&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `featured_vids`
--

CREATE TABLE `featured_vids` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `featured_vids`
--

INSERT INTO `featured_vids` (`id`, `title`, `description`, `url`) VALUES
(8, 'Beyonce - Halo', '<p>My favorite Beyonce song. I ain''t even part of the Beyhive, but gotta admit she is the queen.</p>', 'https://www.youtube.com/embed/bnVUHWCynig'),
(9, 'Lady Leshurr - Queen''s Speech', '<p>Why isn''t this woman famous? Her song was featured in a Samsung phone commercial! And it''s super catchy!</p>', 'https://www.youtube.com/embed/FyodeHtVvkA'),
(10, 'Ring The Alarm', '<p>Yeah, I like some of the older Beyonce stuff.</p>', 'https://www.youtube.com/embed/eY_mrU8MPfI'),
(12, 'Formation', '<p>Yaassss! Slay!</p>', 'https://www.youtube.com/embed/LrCHz1gwzTo');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `firstname`, `lastname`) VALUES
(1, 'angie', '$2y$12$Ekwt1iwwjmyPJC7IBBVzAeyM0Ac3ZVWLL4laiwMhdwIIZ0vI3Xwa2', 'Angie', 'Siu');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `upload_time` date NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `description`, `upload_time`, `url`) VALUES
(1, 'Always Be My Baby', '<p>My favorite Mariah song. Good vibes, good beat, amazing vocals. Halcyon Mariah days.</p>', '2016-03-08', 'https://www.youtube.com/embed/LfRNRymrv9k'),
(2, 'We Belong Together', '<p>Alas, this was the last good song from Mariah.</p>', '2016-03-24', 'https://www.youtube.com/embed/0habxsuXW4g'),
(4, 'Not Afraid', '<p>When a renewed, revived Eminem came out. Before this, Em wasn''t interested in walking "this road together, through the storm, whatever weather, cold or warm."</p>', '2016-02-10', 'https://www.youtube.com/embed/j5-yKhDd64s'),
(7, 'Daughtry - Home', '<p>I love this song from Daughtry! His voice is great. What a sham he wasn''t the American Idol that season.</p>', '2016-01-28', 'https://www.youtube.com/embed/RRTqgYxWG6A'),
(10, 'A Moment Like This', '<p>Sorry, Kelly, but Leona sang your song better. (Not that you''re not amazing yourself!)</p>', '2016-02-06', 'https://www.youtube.com/embed/i8rg1kreNtk'),
(11, 'Leona Lewis', '<p>She''s amazing! Why isn''t she more popular?</p>', '2016-03-05', 'https://www.youtube.com/embed/1iiRKv_C5Zs'),
(14, 'If I Were A Boy', '<p>A fine song by Queen Bey about the struggles and double standards women endure in relationships in a male-dominated, male-oriented world.</p>', '2016-04-19', 'https://www.youtube.com/embed/AWpsOqh8q0M'),
(18, 'Invincible', '<p>Awesome song by the one and only Kelly Clarkson!</p>', '2016-04-19', 'https://www.youtube.com/embed/xQNqaERUYy4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `featured_vids`
--
ALTER TABLE `featured_vids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `featured_vids`
--
ALTER TABLE `featured_vids`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
