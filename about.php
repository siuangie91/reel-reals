<!DOCTYPE html>
<html lang="en">
    
<head>
    <title>Contact and Find Out About Real Reels - 100% Real Videos</title>    
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
    <?php require_once('_includes/head.php'); ?>
    
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>
<?php require_once('_includes/nav.php'); ?>

<?php require_once("_includes/phpmailer.php"); ?>

<?php 
    echo $session->message;
?>
    <script>
        $(window).load(function() {
            $('#submit-modal').modal('show');
        });
    </script>

<div class="page-content" id="about-page-content">
    
    <h1>ABOUT US</h1>

    <div>
        <?php 
        
        $blurbs = About_text::find_all();
        
        foreach($blurbs as $blurb) {
            echo $blurb->description;    
        }
        
        ?>
    </div>
    
<!--    <img src="http://placehold.it/200x100&text=Logo+200x100" alt="Logo">-->
    <img src="_assets/logo-round.gif" alt="Real Reels" class="logo-round">
    
    <div id="contact-anchor"></div>
    
    <div id="contact-form-container">
        <h1>DROP A LINE</h1>
        
        <form action="" method="post" id="contact-form">
            <div id="error">
                <?php echo $session->message; ?>
            </div>
            <div class="form-group">
                <label for="name">Name: <i class="fa fa-asterisk"></i></label>
                <input type="text" class="form-control" name="name" placeholder="First Last">
            </div>
            <div class="form-group">
                <label for="email">Email: <i class="fa fa-asterisk"></i></label>
                <input type="email" class="form-control" name="email" placeholder="email@address.com">
            </div>
            <div class="form-group">
                <label for="subject">Subject: <i class="fa fa-asterisk"></i></label>
                <input type="text" class="form-control" name="subject" placeholder="Subject of Message">
            </div>
            <div class="form-group">
                <label for="message">Message: <i class="fa fa-asterisk"></i></label>
                <textarea name="message" rows="7" class="form-control" placeholder="Your message..."></textarea>
            </div>
            <div class="form-group">
                <div class="g-recaptcha" 
                     data-sitekey="6LeKaSITAAAAAKsiSehZCKFM_HGtBkQwVhTaF0kY"
                     data-callback="enableSubmit"></div>
            </div>

            <input type="submit" class="btn wide-btn" name="submit" value="Submit" id="frm-submit" disabled>
        </form>
    </div>

</div> <!--- Page content --->

<script>
    function enableSubmit() {
        $('#frm-submit').removeAttr('disabled');
    }
</script>

<?php require_once('_includes/footer.php');
